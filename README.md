[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/sscsalex/cms)

# IEEE SSCS AlexSC Website CMS

CMS managing https://sscsalex.org using [KeystoneJS](https://keystonejs.com).

## Running the Project.

To run this project for the first time run `npm install`, then run `npm run dev`.

Once running, the Keystone Admin UI is reachable via http://localhost:3000/admin. Development admin credentials are:

| Username | Password |
| -------- | -------- |
| admin    | goodluck |
