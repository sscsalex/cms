import React from 'react';
import SSCSLogo from './sscs.png';

export default {
	logo: function SSCSLogoReact() {
		return <img width='220' src={SSCSLogo} alt='IEEE SSCS AlexSC' />;
	}
};
