const { MongoMemoryServer } = require('mongodb-memory-server-core');
const { spawn } = require('child_process');

console.log('Starting MongoDB memory server');
const server = new MongoMemoryServer({
	instance: {
		dbName: 'cms',
		port: 27017
	}
});

server
	.getUri()
	.then(() => {
		console.log('Starting Keystone');
		spawn('npx keystone dev', {
			stdio: 'inherit',
			shell: true,
			env: {
				NODE_ENV: 'development',
				DISABLE_LOGGING: 'true',
				...process.env
			}
		})
			.on('error', console.error)
			.on('exit', server.stop);
	})
	.catch(console.error);
